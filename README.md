# ids721-wk10



## Getting started

In this project, I create a docker image using LLM model to handle input query, and deploy it to AWS Lambda

1. create a new project using `cargo lambda new week10-mini`

2. design the function in main.rs

3. test locally
![](pic/1.jpg)

4. create docker image using  `docker build -t week10-mini .`

5. create a AWS ECR repository, upload the docker image
![](pic/2.png)

6. Using the ECR image, create an AWS Lambda function
![](pic/3.png)
